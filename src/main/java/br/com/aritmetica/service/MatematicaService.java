package br.com.aritmetica.service;

import br.com.aritmetica.DTO.EntradaDTO;
import br.com.aritmetica.DTO.RespostaDTO;
import org.springframework.stereotype.Service;

@Service
public class MatematicaService {

    public RespostaDTO soma(EntradaDTO entradaDTO){
        int numero = 0;
        for (int n : entradaDTO.getNumero()){
            numero += n;
        }
        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;
    }

    public RespostaDTO subtracao(EntradaDTO entradaDTO){
        int numero = 0;
        for (int n : entradaDTO.getNumero()){
            if (n == 0){
                numero += n;
            }else{
                numero -= n;
            }
        }
        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;
    }

    public RespostaDTO multiplicacao(EntradaDTO entradaDTO){
        int numero = 1;
        for (int n : entradaDTO.getNumero()){
            numero *= n;
        }
        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;
    }

    public RespostaDTO divisao(EntradaDTO entradaDTO){
        int numero = 1;
        int resultado = 0;
        for (int n : entradaDTO.getNumero()){
            if(numero < n){
                numero = n;
            }else {
                resultado = numero / n;
            }
        }
        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(resultado);
        return resposta;
    }
}
