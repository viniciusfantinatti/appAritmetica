package br.com.aritmetica.controllers;

import br.com.aritmetica.DTO.EntradaDTO;
import br.com.aritmetica.DTO.RespostaDTO;
import br.com.aritmetica.service.MatematicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collections;

@RestController
@RequestMapping("/matematica")
public class MatematicaController {

    @Autowired
    private MatematicaService matematicaService;

    @PutMapping("/soma")
    public RespostaDTO soma(@RequestBody EntradaDTO entradaDTO){
        validaEntrada(entradaDTO);
        RespostaDTO resposta = matematicaService.soma(entradaDTO);
        return resposta;
    }

    @PutMapping("/subtracao")
    public RespostaDTO subtracao(@RequestBody EntradaDTO entradaDTO){
        validaEntrada(entradaDTO);
        RespostaDTO resposta = matematicaService.subtracao(entradaDTO);
        return resposta;
    }

    @PutMapping("/multiplicacao")
    public RespostaDTO multiplicacao(@RequestBody EntradaDTO entradaDTO){
        validaEntrada(entradaDTO);
        RespostaDTO resposta = matematicaService.subtracao(entradaDTO);
        return resposta;
    }

    @PutMapping("/divisao")
    public RespostaDTO divisao(@RequestBody EntradaDTO entradaDTO){
        validaEntrada(entradaDTO);
//        validaEntradaDesc(entradaDTO);
        RespostaDTO resposta = matematicaService.subtracao(entradaDTO);
        return resposta;
    }

    public void validaEntrada(@RequestBody EntradaDTO entradaDTO) {
        if (entradaDTO.getNumero().size() <= 1) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie pelo menos 2 numeros para poder efetuar calculos!");
        }

        for (int x : entradaDTO.getNumero()) {
            if (x <= 0) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Só é permitido apenas numeros naturais maiores que ZERO!");
            }
        }
    }

    public void validaEntradaDesc(@RequestBody EntradaDTO entradaDTO) {
        int numeroAnterior = 0;
        for (int x : entradaDTO.getNumero()){
            if(x > numeroAnterior){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "A lista de numeros deve estar ordenada de forma decrescente");
            }else{
                numeroAnterior = x;
            }
        }
    }
}
