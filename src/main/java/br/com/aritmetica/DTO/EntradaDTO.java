package br.com.aritmetica.DTO;

import java.util.List;

public class EntradaDTO {
    private List<Integer> numeros;

    public EntradaDTO() {
    }

    public List<Integer> getNumero() {
        return numeros;
    }

    public void setNumeros(List<Integer> numeros) {
        this.numeros = numeros;
    }
}

